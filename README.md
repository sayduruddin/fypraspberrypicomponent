# FYPRaspberryPiComponent

This repository will contain the Raspberry Pi code that will be used to collect data from 
the sensors connected to the Pi. 

Make sure to install the libraries needed.

The libraries needed can be found on the official Pimoroni website
for the Enviro+ board and the Plantower PM sensor.

Once the necessary libraries are installed, the script can be run
with 'python project.py' provided Python is already installed. 

