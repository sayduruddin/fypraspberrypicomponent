#!/usr/bin/env python3

import time, math
from pms5003 import PMS5003, ReadTimeoutError
from enviroplus import gas
import logging
import requests
import json

# importing all libraries needed for LCD display
import ST7735
from PIL import Image, ImageDraw, ImageFont
from fonts.ttf import RobotoThin as UserFont

## LCD display setup ##
################################
# Create LCD class instance.
# Standard instance setup from the documentation
disp = ST7735.ST7735(
    port=0,
    cs=1,
    dc=9,
    backlight=12,
    rotation=270,
    spi_speed_hz=10000000
)

# Initialize display.
disp.begin()

# Width and height to calculate text position.
WIDTH = disp.width
HEIGHT = disp.height

# New canvas to draw on.
img = Image.new('RGB', (WIDTH, HEIGHT), color=(0, 0, 0))
draw = ImageDraw.Draw(img)

# Text settings.
font_size = 18
font = ImageFont.truetype(UserFont, font_size)
text_colour = (255, 255, 255)
back_colour = (0, 0, 0)

message = "Running Air Quality\nMonitoring\nscript"

# draw.textsize returns the size of the given text in pixels, multiline does the same
#size_x, size_y = draw.textsize(message, font)
size_x, size_y = draw.multiline_textsize(message, font)

# Calculate text position
x = (WIDTH - size_x) / 2
y = (HEIGHT / 2) - (size_y / 2)


# Draw background rectangle and write text.
draw.rectangle((0, 0, 160, 80), back_colour)
#draw.text((x, y), message, font=font, fill=text_colour)
draw.multiline_text((x, y), message, font=font, fill=text_colour)
disp.display(img)

###############################

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

logging.info(""" This is calculating NH3, NO2 (Oxidising), CO (Reducing), PM2.5 and PM10 values.

Press Ctrl+C to exit!

""")

## Particulate matter component set up and obtaining readings ##
#############################

pms5003 = PMS5003()

reductionr0 = 200000
oxidisingr0 = 20000
nh3r0 = 750000

try:
    while True:
        readings = gas.read_all()  #this is the actual example given
        reading1 = gas.read_nh3()  #i can obtain nh3 this way, can obtain others similarly
        reading2 = gas.read_oxidising()
        reading3 = gas.read_reducing() #can use round function
        pmReading = pms5003.read()
        pm2_5 = pmReading.pm_ug_per_m3(2.5)
        pm10 = pmReading.pm_ug_per_m3(10)
        
        reductionppm = round(math.pow(10, -1.25 * math.log10(reading3/reductionr0) + 0.64 ), 4)
        oxppm = round(math.pow(10, math.log10(reading2/oxidisingr0) - 0.8129 ), 4)
        nh3ppm = round(math.pow(10, -1.8 * math.log10(reading1/nh3r0) - 0.163 ), 4)
        #logging.info(readings)
        #print("NH3 value: ",reading1)
        #print("Oxidising NO2 value: ",reading2)
        #print("Reducing CO value(Ohms): ",reading3)
        print("Reducing CO value(ppm): ", reductionppm)
        print("Oxidising NO2 value(ppm): ", oxppm)
        print("NH3 value(ppm): ", nh3ppm)
        print("PM2.5 ug per m3: ", pm2_5)
        print("PM10 ug per m3: ", pm10)
        
        print("End of readings")
        
        # requests will send the following JSON data to firebase URL  
        data = {'PM10': pm10,
                'PM2_5': pm2_5,
                'NH3': nh3ppm,
                'NO2': oxppm,
                'CO': reductionppm
                }
        url = "https://air-quality-monitoring-ae38b-default-rtdb.firebaseio.com/rpi.json"
        x = requests.patch(url, data=json.dumps(data))
        
        # need to set sleep to 60s so only a result is sent every time period
        time.sleep(60.0)
except KeyboardInterrupt:
        disp.set_backlight(0)
        pass

